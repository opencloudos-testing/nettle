#!/usr/bin/env python3
# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


class Subsystem(object):

    """
    Base subsystem. Provides access to general files, existing in all cgroups
    and means to get/set properties.

    """
    CONFIGS = {}
    STATS = {}
    CONTROLS = {}
    NAME = None

    def __init__(self, cgroup_name):
        self.name = self.NAME

    def get_init_parameters(self, parent_configs):
        return {}
