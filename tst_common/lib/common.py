#!/usr/bin/env python3
# coding: utf-8
# Time: 2022-04-19 22:26:42
# Desc: Python用例公共模块

import abc
import getpass
import os
import platform
import re
import shutil
import socket
import subprocess
import sys
import tempfile
import time
import traceback
import __main__ as main

TST_PASS = 0
TST_FAIL = 1
TST_INIT = 2
TST_SKIP = 3
TST_MANUAL = 4
TST_TIMEOUT = 5
tst_tc_stat_dict = {
    TST_PASS: 'TST_PASS',
    TST_FAIL: 'TST_FAIL',
    TST_INIT: 'TST_INIT',
    TST_SKIP: 'TST_SKIP',
    TST_MANUAL: 'TST_MANUAL',
    TST_TIMEOUT: 'TST_TIMEOUT',
}


def command(cmd, timeout=None):
    proc = subprocess.Popen(cmd, shell=True, encoding='utf-8')
    try:
        proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired:
        proc.kill()
    return proc.wait()


def command_output(cmd, timeout=None):
    proc = subprocess.Popen(cmd, shell=True, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs = ""
    errs = ""
    try:
        outs, errs = proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired:
        proc.kill()
    code = proc.wait()
    return outs, errs, code


def command_quiet(cmd, timeout=None):
    proc = subprocess.Popen(cmd, shell=True, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired:
        proc.kill()
    code = proc.wait()
    return code


def get_yum_provide(filename):
    outs, errs, code = command_output(f'yum provides {filename}')
    print(f'stdout:\n{outs}')
    print(f'stderr:\n{errs}')
    print(f'status: {code}')
    if code != 0 or outs is None:
        return None
    for line in outs.splitlines():
        match = re.match(r'^([-\w.]+)-\d(\.\d+)+-.* : ', line)
        if match:
            package = match.groups()[0]
            print(f'get file {filename} provide: {package}')
            return package
    return None


def try_install_command(cmd, package=None):
    if command_quiet(f'which {cmd}') == 0:
        return True
    if package is None:
        package = get_yum_provide(cmd)
    if package is not None:
        command(f'yum install -y {package}')
    if command(f'which {cmd}') == 0:
        return True
    return False


def get_kernel_release():
    return platform.release()


def get_system_boot_time():
    with open('/proc/uptime', 'r') as f:
        uptime = float(f.readline().split()[0])
    now_time = time.time()
    boot_time = now_time - uptime
    result = boot_time

    tsuite_tmpdir = os.path.join('/tmp', f'.tsuite-{getpass.getuser()}')
    if not os.path.exists(tsuite_tmpdir):
        os.makedirs(tsuite_tmpdir, mode=0o755, exist_ok=True)
    boot_time_file = os.path.join(tsuite_tmpdir, 'boot-time')
    if os.path.exists(boot_time_file):
        # 如果系统启动的时间已经有记录，那么看看是否同一次启动
        with open(boot_time_file, 'r') as f:
            old_boot_time = float(f.readline())
        # 如果记录的时间和当前计算的时间差别不大，那么就认为是同一次启动
        if abs(old_boot_time - boot_time) <= 10:
            result = old_boot_time
        else:
            with open(boot_time_file, 'w') as f:
                f.write(f'{boot_time}')
    else:
        with open(boot_time_file, 'w') as f:
            f.write(f'{boot_time}')
    return 'boot-time.' + time.strftime('%Y%m%d-%H%M%S', time.localtime(result))


def get_timestamp_ms():
    return int(time.time() * 1000)


def get_os_release():
    release = None
    if os.path.isfile('/etc/os-release'):
        with open('/etc/os-release', 'r') as f:
            for line in f.readlines():
                if re.match(r'PRETTY_NAME=', line, re.IGNORECASE):
                    release = re.sub(r'[^\w.]+', '-', line.split('=')[1].strip())
    if os.path.isfile('/etc/tencentos-release'):
        with open('/etc/tencentos-release', 'r') as f:
            for line in f.readlines():
                if re.match(r'TencentOS', line, re.IGNORECASE):
                    release = re.sub(r'[^\w.]+', '-', line.strip())
    if os.path.isfile('/etc/motd'):
        with open('/etc/motd', 'r') as f:
            for line in f.readlines():
                if re.match(r'version', line, re.IGNORECASE):
                    release = re.sub(r'[^\w.]+', '-', line.strip())
                if re.match(r'tlinux', line, re.IGNORECASE):
                    release = re.sub(r'[^\w.]+', '-', line.strip())
    if release is None:
        return 'Unknown-Linux'
    return release.strip('-')


def get_tst_test_env_ip():
    outs, _, _ = command_output('ifconfig eth1')
    eth1_ip = get_ip_of_ifconfig(outs)
    # 测试环境的eth1都是192.168.x.x
    if (eth1_ip is None) or (not eth1_ip.startswith('192.168.')):
        return None
    env_main_ip_1 = re.sub(r'\.\d{1,3}$', '.11', eth1_ip)
    # 获取测试环境主环境的IP
    outs, _, _ = command_output(f'ssh root@{env_main_ip_1} ifconfig eth0', timeout=3)
    env_main_ip_0 = get_ip_of_ifconfig(outs)
    if env_main_ip_0 is None:
        return None
    return [env_main_ip_0, eth1_ip]


def get_ip_of_ifconfig(ifconfig_outs: str):
    if ifconfig_outs is None:
        return None
    if not isinstance(ifconfig_outs, str):
        return None
    for line in ifconfig_outs.splitlines():
        if 'netmask' not in line:
            continue
        return re.search(r'\b(?:\d{1,3}\.){3}\d{1,3}\b', line).group()
    return None


def get_host_ip():
    """
    这个方法是目前见过最优雅获取本机服务器的IP方法了。没有任何的依赖，也没有去猜测机器上的网络设备信息。
    而且是利用 UDP 协议来实现的，生成一个UDP包，把自己的 IP 放如到 UDP 协议头中，然后从UDP包中获取本机的IP。
    这个方法并不会真实的向外部发包，所以用抓包工具是看不到的。但是会申请一个 UDP 的端口，所以如果经常调用也会比较耗时的，
    这里如果需要可以将查询到的IP给缓存起来，性能可以获得很大提升。

    作者：钟翦
    链接：https://www.zhihu.com/question/49036683/answer/1243217025
    来源：知乎
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

    :return:
    """
    _local_ip = None
    s = None
    try:
        if not _local_ip:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 80))
            _local_ip = s.getsockname()[0]
        return _local_ip
    finally:
        if s:
            s.close()


def command_to_filename(cmd: str):
    return re.sub(r'\W+', '.', cmd) + '.txt'


def get_commands_logs(cmd_list, logs_dir, timeout=60):
    start_time = time.time()
    exec_list = list()
    index = 1
    for cmd in cmd_list:
        # print(f'{index} execute command: {cmd}')
        cmd_dict = {
            'index': index,
            'command': cmd,
            'timeout': timeout,
            'start_time': time.time(),
            'proc': subprocess.Popen(cmd, shell=True, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE),
            'log_file': os.path.join(os.path.realpath(logs_dir), command_to_filename(cmd)),
            'end_time': None,
            'cost': None,
            'stdout': None,
            'stderr': None,
            'status': None
        }
        exec_list.append(cmd_dict)
        index += 1

    # 检查命令是否执行完
    while True:
        all_command_ok = True
        if time.time() - start_time > timeout:
            break
        for proc_dict in exec_list:
            if proc_dict['status'] is not None:
                continue
            try:
                proc_dict['stdout'], proc_dict['stderr'] = proc_dict['proc'].communicate(timeout=1)
            except subprocess.TimeoutExpired:
                all_command_ok = False
                continue
            proc_dict['status'] = proc_dict['proc'].returncode
            proc_dict['end_time'] = time.time()
            proc_dict['cost'] = proc_dict['end_time'] - proc_dict['start_time']
            # print(f'{proc_dict["index"]} command {proc_dict["command"]} end, status {proc_dict["status"]}')
        if all_command_ok:
            break

    # 记录命令执行结果，还没有结束的命令直接终止
    for proc_dict in exec_list:
        if proc_dict['status'] is None:
            proc_dict['proc'].kill()
            proc_dict['stdout'], proc_dict['stderr'] = proc_dict['proc'].communicate(timeout=1)
            proc_dict['status'] = proc_dict['proc'].returncode
            proc_dict['end_time'] = time.time()
            proc_dict['cost'] = proc_dict['end_time'] - proc_dict['start_time']
            print(f'{proc_dict["index"]} command {proc_dict["command"]} killed, status {proc_dict["status"]}')
        with open(file=proc_dict['log_file'], mode='a', buffering=1, encoding='utf-8') as f:
            f.write('=' * 60 + '\n')
            f.write(f'### command: {proc_dict["command"]}\n')
            f.write(f'### timeout: {proc_dict["timeout"]}\n')
            f.write(f'### start_time: '
                    f'{time.strftime("%Y-%m-%d %H:%M:%S %z", time.localtime(proc_dict["start_time"]))}\n')
            f.write(f'### end_time: '
                    f'{time.strftime("%Y-%m-%d %H:%M:%S %z", time.localtime(proc_dict["end_time"]))}\n')
            f.write(f'### cost: {proc_dict["cost"]}\n')
            f.write(f'### log_file: {proc_dict["log_file"]}\n')
            f.write(f'### status: {proc_dict["status"]}\n')
            f.write('=' * 60 + '\n')
            f.write('### stdout:\n')
            f.write(proc_dict["stdout"])
            f.write('=' * 60 + '\n')
            f.write('### stderr:\n')
            f.write(proc_dict["stderr"])
            f.write('=' * 60 + '\n')
    print(f'### all command cost {time.time() - start_time}')


def sysinfo(log_dir, install_command=False, merge_files=True):
    command_list = [
        "uname -a",
        "ulimit -a",
        "lscpu",
        "lspci",
        "lspci -k",
        "lspci -vv",
        "lspci -t -vv -nn",
        "lsscsi",
        "lsscsi --list --verbose --long",
        "lsusb",
        "lsusb --tree",
        "lshw -short",
        "lsmod",
        "route -n",
        "ifconfig -a",
        "dmidecode",
        "udevadm info --export-db",
        "ipmitool sdr",
        "ipmitool fru",
        "ipmitool sensor",
        "ipmitool sel",
        "fdisk -l",
        "mount",
        "df -P",
        "lsblk",
        "lsblk -O",
        "smartctl --scan"
        "free",
        "numastat -v",
        "numastat -v -m",
        "cpupower --cpu all frequency-info",
        "cpupower --cpu all idle-info",
        "sysctl -a",
        "cat /etc/os-release",
        "cat /etc/system-release",
        "cat /etc/centos-release",
        "cat /etc/redhat-release",
        "cat /etc/system-release",
        "cat /etc/tencentos-release",
        "cat /etc/tlinux-release",
        "cat /proc/cpuinfo",
        "cat /proc/meminfo",
        "cat /proc/buddyinfo",
        "cat /proc/devices",
        "cat /proc/interrupts",
        "cat /proc/iomem",
        "cat /proc/ioports",
        "cat /proc/modules",
        "cat /proc/module_md5_list",
        "zcat /proc/config.gz",
        "zcat /proc/bt_stat",
        "zcat /proc/cgroups",
        "zcat /proc/cmdline",
        "zcat /proc/consoles",
        "zcat /proc/crypto",
        "zcat /proc/devices",
        "zcat /proc/diskstats",
        "zcat /proc/dma",
        "zcat /proc/execdomains",
        "zcat /proc/filesystems",
        "zcat /proc/kallsyms",
        "zcat /proc/loadavg",
        "zcat /proc/loadavg_bt",
        "zcat /proc/locks",
        "zcat /proc/mdstat",
        "zcat /proc/misc",
        "zcat /proc/mtrr",
        "zcat /proc/partitions",
        "zcat /proc/sched_debug",
        "zcat /proc/schedstat",
        "zcat /proc/slabinfo",
        "zcat /proc/softirqs",
        "zcat /proc/stat",
        "zcat /proc/swaps",
        "zcat /proc/timer_list",
        "zcat /proc/uptime",
        "zcat /proc/version",
        "zcat /proc/vmallocinfo",
        "zcat /proc/vmstat",
        "zcat /proc/zoneinfo",
        "dmesg"
    ]
    if os.path.isfile('/proc/net/dev'):
        with open(file='/proc/net/dev', mode='r') as f:
            for line in f.readlines():
                if ':' not in line:
                    continue
                dev = line.split(':')[0].strip()
                command_list.append(f'ethtool {dev}')
                command_list.append(f'ethtool -i {dev}')
    if os.path.isfile('/proc/modules'):
        with open(file='/proc/modules', mode='r') as f:
            for line in f.readlines():
                module = line.split()[0].strip()
                command_list.append(f'modinfo {module}')
    outs, _, _ = command_output('smartctl --scan')
    if outs:
        for line in outs.splitlines():
            dev = line.split()[0].strip()
            command_list.append(f'smartctl --xall {dev}')
            command_list.append(f'hdparm {dev}')

    # 尝试安装命令
    if install_command:
        for cmd in set([c.split()[0] for c in command_list]):
            try_install_command(cmd)

    if not os.path.exists(log_dir):
        os.makedirs(log_dir, mode=0o755, exist_ok=True)
    get_commands_logs(command_list, log_dir)
    # 将多个日志文本文件合并成一个文件
    if merge_files:
        tmp_file = tempfile.mkstemp()[1]
        with open(tmp_file, 'w') as f:
            for txt_file in os.listdir(log_dir):
                txt_path = os.path.join(log_dir, txt_file)
                with open(txt_path, 'r') as tf:
                    f.write(tf.read())
                os.remove(txt_path)
        shutil.move(tmp_file, os.path.join(log_dir, 'sysinfo.txt'))


def get_crash_path():
    result = None
    if not os.path.exists('/etc/kdump.conf'):
        return result
    with open('/etc/kdump.conf', 'r') as f:
        for line in f.readlines():
            if line.startswith('path '):
                result = line.split()[1]
    return result


def safe_repr(obj, short=False):
    max_len = 120
    try:
        result = repr(obj)
    except Exception:
        result = object.__repr__(obj)
    if not short or len(result) < max_len:
        return result
    return result[:max_len] + ' [truncated]...'


def set_env(env, value):
    if os.environ.get(env):
        return
    os.environ[env] = f"{value}"


def set_env_force(env, value):
    if os.environ.get(env):
        print(f"the env {env} has value {os.environ.get(env)}, now set to {value}")
    os.environ[env] = value


def get_env(env):
    return os.environ.get(env)


def get_env_default(env, default):
    return os.environ.get(env, default)


def _is_main_process():
    return int(get_env_default('TST_TC_PID', '0')) == os.getpid()


class ExceptionSkip(Exception):
    def __init__(self, message=""):
        super(ExceptionSkip, self).__init__(self)
        self.message = f'{message}'

    def __str__(self):
        return self.message


def tcstat_to_str(tcstat):
    tst_tc_stat_dict.get(tcstat, 'UNKNOWN')


class TestCase:
    def __init__(self, *args):
        # 用例文件路径
        self.tc_path = os.path.realpath(main.__file__)
        # 用例名
        self.tc_name = self._get_tc_attr('用例名称')
        # 执行用例文件时的目录
        self.exec_cwd = os.getcwd()
        # TST_TC_CWD 测试用例CWD
        self.tst_tc_cwd = os.path.dirname(self.tc_path)
        set_env('TST_TC_CWD', self.tst_tc_cwd)
        # TST_TC_PID 测试用例主进程pid
        self.tst_tc_pid = os.getpid()
        set_env('TST_TC_PID', self.tst_tc_pid)
        # 用例执行状态
        self._tcstat = TST_INIT
        # 用例退出码，0表示用例PASS或SKIP，其他表示异常
        self._exit_code = 0
        # TST_TS_TOPDIR 测试套顶层目录
        self.tst_ts_topdir = self._get_ts_topdir()
        set_env('TST_TS_TOPDIR', self.tst_ts_topdir)
        # TST_COMMON_TOPDIR common顶层目录
        self.tst_common_topdir = self._get_common_topdir()
        set_env('TST_COMMON_TOPDIR', self.tst_common_topdir)
        # 标记tc_setup是否被调用
        self._tc_setup_called = False
        # TST_TS_SYSDIR 测试套公共运行目录
        self.tst_ts_sysdir = None if self.tst_ts_topdir is None \
            else os.path.join(self.tst_ts_topdir, 'logs', '.ts.sysdir')
        set_env('TST_TS_SYSDIR', self.tst_ts_sysdir)
        # TST_TC_SYSDIR 测试用例管理用临时目录
        self.tst_tc_sysdir = None if self.tst_ts_topdir is None \
            else os.path.join(self.tst_ts_topdir, 'logs', 'testcase', f'.tc.{self.tst_tc_pid}.sysdir')
        set_env('TST_TC_SYSDIR', self.tst_tc_sysdir)
        self.is_auto = True

    def dbg(self, message):
        print(message, file=sys.stderr)

    def msg(self, message):
        print(message, file=sys.stderr)

    def err(self, message):
        print(message, file=sys.stderr)
        self._fail(message)

    def upload_debug_file(self, *files):
        """
        上传指定文件用于用例失败后的调试分析
        :param files: 要上传的文件
        :return: 无
        """
        log_dir = os.path.join(self.tst_tc_sysdir, 'log_files')
        os.makedirs(log_dir, mode=0o755, exist_ok=True)
        for f in files:
            if not os.path.exists(f):
                self.msg(f'the file to upload not exist: {f}')
                continue
            try:
                if os.path.isfile(f):
                    shutil.copy(src=f, dst=os.path.join(log_dir, os.path.basename(f)))
                elif os.path.isdir(f):
                    shutil.copytree(src=f, dst=os.path.join(log_dir, os.path.basename(f)))
                else:
                    self.msg(f'unknown file type: {f}')
            except Exception:
                self.msg(f'copy the file {f} to {log_dir} fail')
                self.msg(traceback.format_exc())

    def _upload_debug_file(self):
        flag = get_env('TST_UPLOAD_FAIL_LOG')
        if flag == '1' or flag == 'yes' or flag == 'true':
            self.msg('upload test fail log to service')
        else:
            return
        upload_dir = os.path.join(self.tst_tc_sysdir, 'upload')
        os.makedirs(upload_dir, mode=0o755, exist_ok=True)
        command(f'python3 {os.path.join(self.tst_common_topdir, "cmd", "upload-logs.py")} {upload_dir} {self.tc_name}')

    def _get_tc_attr(self, attr):
        with open(file=self.tc_path, mode='r', encoding='utf-8') as f:
            for line in f.readlines():
                if f"@{attr}:" not in line:
                    continue
                return re.sub(f'.*@{attr}:', '', line).strip()
        return None

    def _get_ts_topdir(self):
        result = self.tst_tc_cwd
        while True:
            if os.path.isdir(os.path.join(result, 'cmd')) and os.path.isdir(os.path.join(result, 'testcase')) and \
                    os.path.isfile(os.path.join(result, 'tsuite')):
                if os.path.isdir(os.path.join(result, '..', 'tst_common')):
                    return os.path.dirname(result)
                return result
            next_path = os.path.dirname(result)
            if next_path == result:
                return None
            result = next_path

    def _get_common_topdir(self):
        if self.tst_ts_topdir is None:
            return None
        if os.path.isdir(os.path.join(self.tst_ts_topdir, 'tst_common')):
            return os.path.join(self.tst_ts_topdir, 'tst_common')
        return self.tst_ts_topdir

    def _set_tcstat(self, stat):
        self._tcstat = stat

    def _get_tcstat(self):
        return self._tcstat

    def _get_tcstat_str(self):
        return tcstat_to_str(self._tcstat)

    def _fail(self, message=None):
        if self._tcstat != TST_FAIL:
            self.msg("the testcase first fail here")
            self._set_tcstat(TST_FAIL)
            raise AssertionError(message)

    def _pass(self):
        if self._get_tcstat() == TST_INIT:
            self._set_tcstat(TST_PASS)

    def skip_test(self, message=None):
        """
        标记用例为SKIP状态（当用例不需要测试时）
        :param message: 要输出的信息
        :return: None
        示例:
            skip_test(message='内核CONFIG_XXX未开，系统不支持此测试')
        """
        if self._get_tcstat() in (TST_PASS, TST_INIT, TST_SKIP):
            self._set_tcstat(TST_SKIP)
            raise ExceptionSkip(f'set testcase SKIP: {message}')
        else:
            self.msg(f'set testcase SKIP fail: {message}')
            self._fail(f'the testcase stat is {self._get_tcstat_str()}, can\'t set to SKIP')

    def skip_if_true(self, expr, message=None):
        """
        当表达式返回真或命令执行成功时，用例不满足测试条件，终止测试
        :param expr: 要判断的表达式
        :param message: 要输出的信息
        :return: None
        示例:
            skip_if_true(1 == 2)
        """
        if expr:
            self.skip_test(f'skip_if_true -> {safe_repr(expr)}: {message}')

    def skip_if_false(self, expr, message=None):
        """
        当表达式返回假或命令执行失败，用例不满足测试条件，终止测试
        :param expr: 要判断的表达式
        :param message: 要输出的信息
        :return: None
        示例:
            skip_if_false(1 == 2)
        """
        if not expr:
            self.skip_test(f'skip_if_false -> {safe_repr(expr)}: {message}')

    def assert_true(self, expr, message=None):
        """
        断言表达式返回真
        :param expr: 要断言的表达式
        :param message: 断言要输出的信息
        :return: None
        示例:
            assert_true(1 == 2)
            assert_true(a >= b)
        """
        if not expr:
            self.msg(f"{safe_repr(expr)} is not true: {message}")
            self._fail(message)
        else:
            self._pass()

    def assert_false(self, expr, message=None):
        """
        断言表达式返回假
        :param expr: 要断言的表达式
        :param message: 断言要输出的信息
        :return: None
        示例:
            assert_false(1 == 2)
            assert_false(a >= b)
        """
        if expr:
            self.msg(f"{safe_repr(expr)} is not false: {message}")
            self._fail(message)
        else:
            self._pass()

    def _tc_setup_common(self, *args):
        # 只有用例的主进程才能执行此函数
        if not _is_main_process():
            return None
        try:
            self.tc_setup_common(*args)
        except Exception:
            raise

    def tc_setup_common(self, *args):
        self.msg("this is TestCase.tc_setup_common")

    def _tc_setup(self, *args):
        # 只有用例的主进程才能执行此函数
        if not _is_main_process():
            return None
        try:
            self._tc_setup_called = True
            self.tc_setup(*args)
        except Exception:
            raise

    def tc_setup(self, *args):
        self.msg("this is TestCase.tc_setup")

    def _do_test(self, *args):
        # 只有用例的主进程才能执行此函数
        if not _is_main_process():
            return None
        try:
            self.do_test(*args)
        except Exception:
            raise

    @abc.abstractmethod
    def do_test(self, *args):
        self.msg("this is TestCase.do_test")

    def _tc_teardown(self, *args):
        # 只有用例的主进程才能执行此函数
        if not _is_main_process():
            return None
        try:
            self.tc_teardown(*args)
        except Exception:
            raise

    def tc_teardown(self, *args):
        self.msg("this is TestCase.tc_teardown")

    def _tc_teardown_common(self, *args):
        # 只有用例的主进程才能执行此函数
        if not _is_main_process():
            return None
        try:
            self.tc_teardown_common(*args)
        except Exception:
            raise

    def tc_teardown_common(self, *args):
        self.msg("this is TestCase.tc_teardown_common")

    def _is_ts_setup_called(self):
        if self.tst_ts_sysdir:
            return os.path.isfile(os.path.join(self.tst_ts_sysdir, 'ts.setup.stat'))
        else:
            return False

    def _get_ts_setup_stat(self):
        if self.tst_ts_sysdir:
            ts_stat_file = os.path.join(self.tst_ts_sysdir, 'ts.setup.stat')
            if not os.path.isfile(ts_stat_file):
                return None
            with open(file=ts_stat_file, mode='r', encoding='utf-8') as f:
                line = f.readline()
                return int(line)
        else:
            return None

    def _get_step_expect(self):
        step_expect = list()
        with open(self.tc_path, 'r') as f:
            for line in f.readlines():
                if "@测试步骤:" in line:
                    step_expect.append(re.sub('@测试步骤:' + f'.*@测试步骤:', '', line).strip())
                elif "@预期结果:" in line:
                    step_expect.append(re.sub('@预期结果:' + f'.*@预期结果:', '', line).strip())
        return '\n'.join(step_expect)

    def _shou_case_attr(self):
        attr_list = [
            '用例ID',
            '用例名称',
            '用例级别',
            '用例标签',
            '扩展属性',
            '用例类型',
            '自动化',
            '超时时间',
            '用例描述',
            '预置条件',
        ]
        self.msg("==================== vvv 用例属性 vvv ====================")
        for case_attr in attr_list:
            self.msg(f'{case_attr}: {self._get_tc_attr(case_attr)}')
        self.msg(self._get_step_expect())
        self.msg("==================== ^^^ 用例属性 ^^^ ====================")

    def _get_tst_result_file(self):
        tst_result_file = os.path.join(self.tst_ts_topdir, "logs/report.result")
        os.environ['TST_RESULT_FILE'] = tst_result_file
        if not os.path.isfile(tst_result_file):
            with open(tst_result_file, 'w') as f:
                f.write("TESTSUITE\n")
                f.write(f"suite-name: {os.path.basename(self.tst_ts_topdir)}\n")
                f.write(f"suite-start-time: {get_timestamp_ms()}\n")
                f.write("suite-end-time:\n\n")
        return tst_result_file

    def tst_main(self, *args):
        tst_result_file = self._get_tst_result_file()
        tst_tc_name = self._get_tc_attr('用例名称')
        with open(tst_result_file, 'a') as f:
            f.write("TESTCASE\n")
            f.write(f"case-name:  {tst_tc_name}\n")
            f.write(f"case-id: {self._get_tc_attr('用例ID')}\n")
            f.write("case-type: case-type\n")
            f.write(f"case-level: {self._get_tc_attr('用例级别')}\n")
            f.write(f"case-label: {self._get_tc_attr('用例标签')}\n")
            f.write(f"case-steps: {self._get_tc_attr('测试步骤')}\n")
            f.write(f"case-result-id: {tst_tc_name}-result\n")
            f.write(f"case-start-time: {get_timestamp_ms()}\n")
        time_start = int(time.monotonic() * 1000000000)
        if self._is_ts_setup_called():
            self.msg(f"tsuite setup executed, stat is {self._get_ts_setup_stat()}")
        else:
            self.msg("tsuite setup may not executed")

        self._shou_case_attr()
        # 如果是手动用例，那么不需要执行
        if self._get_tc_attr('自动化') == '0':
            self.is_auto = False
        if not self.is_auto:
            self.msg("this is a manual testcase, need to be executed manually")
            self.msg(f"RESULT : {self.tc_name} ==> [  MANUAL  ]")
            return 0

        try:
            self._tc_setup_common(*args)
            self.msg("call _tc_setup_common success")
            self._tc_setup(*args)
            self.msg("call _tc_setup success")
            self._do_test(*args)
            self.msg("call _do_test success")
        except ExceptionSkip as e:
            self._set_tcstat(TST_SKIP)
            self.msg(e.message)
        except Exception:
            self._set_tcstat(TST_FAIL)
            self.msg(traceback.format_exc())
        finally:
            try:
                if self._tc_setup_called:
                    self._tc_teardown(*args)
                else:
                    self.msg("the tc_setup not called, so tc_teardown ignore")
                self._tc_teardown_common(*args)
            except ExceptionSkip as e:
                self._set_tcstat(TST_SKIP)
                self.msg(e.message)
            except Exception:
                self._set_tcstat(TST_FAIL)
                self.msg(traceback.format_exc())

        # TCase自动化执行框架需要用这个输出判断用例是否支持完
        self.msg("Global test environment tear-down")
        if self._get_tcstat() == TST_PASS:
            self.msg(f"RESULT : {self.tc_name} ==> [  PASSED  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: PASS\n")
            self._exit_code = 0
        elif self._get_tcstat() == TST_FAIL:
            self._upload_debug_file()
            self.msg(f"RESULT : {self.tc_name} ==> [  FAILED  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: FAIL\n")
            self._exit_code = 1
        elif self._get_tcstat() == TST_INIT:
            self._upload_debug_file()
            self.msg(f"RESULT : {self.tc_name} ==> [  NOTEST  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: NOTEST\n")
            self._exit_code = 1
        elif self._get_tcstat() == TST_SKIP:
            self.msg(f"RESULT : {self.tc_name} ==> [  SKIP  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: SKIP\n")
            self._exit_code = 0
        elif self._get_tcstat() == TST_MANUAL:
            self.msg(f"RESULT : {self.tc_name} ==> [  MANUAL  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: MANUAL\n")
            self._exit_code = 0
        elif self._get_tcstat() == TST_TIMEOUT:
            self.msg(f"RESULT : {self.tc_name} ==> [  TIMEOUT  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: TIMEOUT\n")
            self._exit_code = 1
        else:
            self._upload_debug_file()
            self.msg(f"RESULT : {self.tc_name} ==> [  UNKNOWN  ]")
            with open(tst_result_file, 'a') as f:
                f.write("case-result: UNKNOWN\n")
            self._exit_code = 1
        time_end = int(time.monotonic() * 1000000000)
        # Allure.get_report(self.tc_name)
        self.msg(f'cost {(time_end - time_start) / 1000000000:.9f}')
        # 写入用例执行结束时间
        with open(tst_result_file, 'a') as f:
            f.write(f"case-end-time:  {get_timestamp_ms()}\n\n")
        # allure数据源文件生成
        kwargs = {
            "tst_ts_topdir": self.tst_ts_topdir,
            "case_name": tst_tc_name,
            "tst_result_file": tst_result_file,
            "case_id": self._get_tc_attr('用例ID'),
            "case_desc": self._get_tc_attr('用例描述')
        }
        Allure.make_allure_json(**kwargs)
        exit(self._exit_code)


class Allure(object):
    def __init__(self):
        pass

    @staticmethod
    def _get_json_from_result_file(tst_result_file):
        with open(tst_result_file, 'r') as f:
            lines = f.readlines()
        start_time = re.search(r'case-start-time: (.*)', lines[-4]).group(1)
        end_time = re.search(r'case-end-time: (.*)', lines[-2]).group(1)
        allure_case_result = re.search(r'case-result: (.*)', lines[-3]).group(1)
        allure_case_result = "passed" if allure_case_result == "PASS" else \
            "failed" if allure_case_result == "FAIL" else \
                "skipped" if allure_case_result == "SKIP" else \
                    "broken"
        return start_time, end_time, allure_case_result

    @classmethod
    def make_allure_json(cls, **kwargs):
        tst_allure_json_file = os.path.join(kwargs.get('tst_ts_topdir'), "logs/allure_data",
                                            f"{kwargs.get('case_name')}-result.json")
        start_time, end_time, allure_case_result = cls._get_json_from_result_file(kwargs.get('tst_result_file'))
        try:
            if allure_case_result not in ["passed", "skipped"]:
                image_url = "xxx"
                with open(tst_allure_json_file, 'w') as f:
                    f.write("{\n")
                    f.write(f"  \"uuid\": \"{kwargs.get('case_id')}\",\n")
                    f.write(f"  \"name\": \"{kwargs.get('case_name')}\",\n")
                    f.write(f"  \"description\": \"{kwargs.get('case_desc')}\",\n")
                    f.write(f"  \"status\": \"{allure_case_result}\",\n")
                    f.write(f"  \"start\": \"{start_time}\",\n")
                    f.write(f"  \"stop\": \"{end_time}\",\n")
                    f.write("   \"labels\": [{\n")
                    f.write(f"    \"name\": \"story\",\n")
                    f.write(f"    \"value\": \"{os.path.basename(kwargs.get('tst_ts_topdir'))}\"\n")
                    f.write("  }],\n")
                    f.write("   \"parameters\": [{\n")
                    f.write(f"    \"name\": \"os信息\",\n")
                    f.write(f"    \"value\": \"{os.popen('cat /etc/system-release').read().strip()}\"\n")
                    f.write("  },\n")
                    f.write("  {\n")
                    f.write(f"    \"name\": \"内核版本\",\n")
                    f.write(f"    \"value\": \"{os.popen('uname -r').read().strip()}\"\n")
                    f.write("  },\n")
                    f.write("  {\n")
                    f.write(f"    \"name\": \"架构类型\",\n")
                    f.write(f"    \"value\": \"{os.popen('arch').read().strip()}\"\n")
                    if os.path.exists("/etc/tst-env.conf"):
                        with open("/etc/tst-env.conf", "r") as d:
                            for line in d:
                                if "node_id=" in line:
                                    node_id = line.split("=")[1].strip()
                                    f.write("  },\n")
                                    f.write("  {\n")
                                    f.write("    \"name\": \"ndeploy节点\",\n")
                                    f.write(f"    \"value\": \"ndeploy ssh vm -n {node_id}\"\n")
                                elif "image_url=" in line:
                                    image_url = line.split("=")[1].strip()
                    f.write("  },\n")
                    f.write("  {\n")
                    f.write("    \"name\": \"复现步骤\",\n")
                    f.write(
                        f"    \"value\": \"(1)：ndeploy new vm -i {image_url}  "
                        f"(2)：git clone --recurse-submodules git@git.woa.com:tlinux/TST/{os.path.basename(os.path.dirname(kwargs.get('tst_ts_topdir')))}  "
                        f"(3)：cd {os.path.basename(os.path.dirname(kwargs.get('tst_ts_topdir')))}/{os.path.basename(kwargs.get('tst_ts_topdir'))}  "
                        f"(4)：./tsuite run testcase/{kwargs.get('case_name')}.py\"\n")
                    if image_url != "xxx":
                        f.write("  },\n")
                        f.write("  {\n")
                        f.write("    \"name\": \"镜像地址\",\n")
                        f.write(f"    \"value\": \"{image_url}\"\n")
                    f.write("  }],\n")
                    f.write("   \"attachments\": [{\n")
                    f.write(f"    \"name\": \"log\",\n")
                    f.write(f"    \"source\": \"{kwargs.get('case_name')}.py.log\",\n")
                    f.write("     \"type\": \"text/plain\"\n")
                    f.write("  }]\n")
                    f.write("}\n")
        except Exception as e:
            print(f"make_allure_json fail: {e}")
