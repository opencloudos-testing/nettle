#!/bin/bash
###############################################################################
# @用例ID: 20220410-152859-724647329
# @用例名称: test_shell_testcase
# @用例级别: 3
# @用例标签:
# @扩展属性:
# @用例类型: 功能测试
# @自动化: 1
# @超时时间: 0
# @用例描述: TODO: 简要描述用例测试的内容
###############################################################################
if [ -z "$TST_TS_TOPDIR" ]; then
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    while [ "$TST_TS_TOPDIR" != "/" ]; do
        [ -f "${TST_TS_TOPDIR}/tsuite" ] && break
        TST_TS_TOPDIR="$(dirname "$TST_TS_TOPDIR")"
    done
    export TST_TS_TOPDIR
fi
source "${TST_TS_TOPDIR}/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"

    # @预置条件: TODO: 描述测试用例执行需要的预置条件
    # @预置条件: TODO: 可以使用skip_if_false等函数判断条件是否满足，不满足用例将不被执行
    skip_if_false [ $((1 + 1)) -eq 2 ] # TODO: 示例代码，用例中必须删除
    return 0
}

do_test() {
    msg "this is do_test"

    # @测试步骤: TODO: 描述用例测试的操作步骤
    # @预期结果: TODO: 紧跟操作步骤描述该步骤操作后的预期结果，>>> 必须 <<<使用断言判断预期结果
    a=$((1 + 1))             # TODO: 示例代码，用例中必须删除
    assert_true [ $a -eq 2 ] # TODO: 示例代码，用例中必须删除

    # @测试步骤: step 2

    # @测试步骤: step 3
    # @预期结果: expect of step 3

    return 0
}

tc_teardown() {
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
