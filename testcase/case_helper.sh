#!/bin/bash
###############################################################################
# @用例ID: 20231201-155107-724647329
# @用例名称: test_shell_testcase
# @用例级别: 3
# @用例标签:
# @扩展属性:
# @用例类型: 功能测试
# @自动化: 1
# @超时时间: 0
# @用例描述: TODO: 简要描述用例测试的内容
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"

    # @预置条件: 安装 nettle
    assert_true yum install -y nettle
    return 0
}

do_test() {
    msg "this is do_test"

    # @测试步骤: 执行 --help 
    # @预期结果: 返回正常
    nettle-hash --help
    assert_true [ $? -eq 0 ]
    return 0
}

tc_teardown() {
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
