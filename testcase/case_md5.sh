#!/bin/bash
###############################################################################
# @用例ID: 20231201-145059-724647329
# @用例名称: case_md5
# @用例级别: 3
# @用例标签:
# @扩展属性:
# @用例类型: 功能测试
# @自动化: 1
# @超时时间: 0
# @用例描述: nettle md5 算法确认
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/tst_common/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"

    # @预置条件: 安装 nettle
    assert_true yum install -y nettle
    return 0
}

do_test() {
    msg "this is do_test"

    # @测试步骤: 新建一个文件，对其进行加密&解密, 然后对比是否正常
    echo "this is nettle hash test" > ${TST_TS_TOPDIR}/testcase/plaintext.txt

    # 使用nettle-hash计算MD5值
    nettle_md5=$(nettle-hash -a md5 ${TST_TS_TOPDIR}/testcase/plaintext.txt | awk '{print $2$3}')

    # 使用md5sum计算MD5值
    md5sum_md5=$(md5sum ${TST_TS_TOPDIR}/testcase/plaintext.txt | awk '{print $1}')
    # @预期结果: 结果一致
    # 比较两个命令的输出
    assert_true [ "$nettle_md5" == "$md5sum_md5" ]
}

tc_teardown() {
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
